import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/subscribe', pathMatch: 'full'},
  { path: 'home', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'subscribe', loadChildren: './subscribe/subscribe.module#SubscribePageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
